class MoviesController < ApplicationController
	include Response
	include ExceptionHandler
	before_action :set_movie, only: [:show, :update, :destroy]
	
	def index
		@movies = Movie.all.order(:name)
		json_response(@movies)
	end
	
	def show
		json_response(@movie)
	end
	
	def create
		@movie = Movie.create!(movie_params)
		json_response(@movie, :created)
	end
	
	def update
		@movie.update!(movie_params)
		json_response(@movie.reload)
	end

	def destroy
		@movie.destroy
		head :no_content
	end
	
	private
	
	def movie_params
		params.permit(:name, :year, :rating, :comment)
	end
	
	def set_movie
		@movie = Movie.find(params[:id])
	end
    
end
